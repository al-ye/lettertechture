<?php

    require_once 'version.php';
    require_once 'base-config.php';

    $_S = 2;
    $_Y = 2020;

    ob_start();

    require ABSPATH . "/src/php/{$_Y}/semester-{$_S}/index.php";

    $html = ob_get_clean();

    file_put_contents(BUILD_DIR . "/{$_Y}/semester-{$_S}/index.html", $html);


