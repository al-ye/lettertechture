<?php

    if (!defined('BUILD_VERSION')) {
        $version = '0.0.0';
        $package = __DIR__ . '/package.json';
        try {
            $package_contents = json_decode(file_get_contents($package));
            $version = $package_contents->version;
        }
        catch(Exception $e) {
            error_log($e->getMessage());
        }

        define('BUILD_VERSION', $version);
    }
