<?php

require_once "./utils.php";

class MajorProject {
    private $id;
    private $year;
    private $semester;
    private $title;
    private $subtitle;
    private $author_firstnames;
    private $author_surname;
    private $description;
    private $supervisor;
    private $award;
    private $external_link;
    private $external_link_text;
    private $media_items;
    private $exhibition;
    private $thumbnail;
    private $thumbnail_animation;

    public function __construct($raw_project) {
        $this->year = $raw_project['year'] ?: '';
        $this->semester = $raw_project['semester'] ?: '';
        $this->title = $raw_project['title'] ?: '';
        $this->subtitle = $raw_project['subtitle'] ?: '';
        $this->author_firstnames = $raw_project['author_firstnames'] ?: '';
        $this->author_surname = $raw_project['author_surname'] ?: '';
        $this->description = $raw_project['description'] ?: '';
        $this->supervisor = $raw_project['supervisor'] ?: '';
        $this->external_link = $raw_project['external_link'] ?: '';
        $this->external_link_text = $raw_project['external_link_text'] ?: $raw_project['external_link'];
        $this->thumbnail = $raw_project['thumbnail'] ?: '';
        $this->thumbnail_animation = $raw_project['thumbnail_animation'] ?: false;
        $this->award = $raw_project['award'] ?: false;

        $media_items = [];
        $num_media_items = 3;
        for ($i = 1; $i <= $num_media_items; $i++) {
            if ($raw_project["media_$i"]) {
                $media_items[] = $raw_project["media_$i"];
            }
        }

        $this->media_items = $media_items;
    }


    public function get_uri() {
        if (!$this->id) {
            $parts = [
                $this->author_firstnames,
                $this->author_surname,
                $this->title
            ];

            $this->id = MajorProject::sanitize(implode(' ', $parts));
        }

        return $this->id;
    }


    public function get_author_display() {
        return $this->author_firstnames . ' ' . $this->author_surname;
    }

    public static function sanitize($string) {
        return string_to_filename($string);


        // $string = trim($string);
        // $string = strtolower($string);
        // // $string = str_replace(["'", " ", $string);
        // $string = preg_replace("/[^[:alnum:]]/", '-', $string);
        // $string = preg_replace("/-{1,}/", '-', $string);
        // // $string = preg_replace("/-\.jpg$/", '.jpg', $string);
        // return $string;
    }

    public static function media_platform($media_item) {

        return "issuu";
    }

    public function get_title() {
        return $this->title;
    }

    public function get_subtitle() {
        return $this->subtitle;
    }
    // public function get_author_firstnames() {
    //     return $this->author_firstnames;
    // }
    // public function get_author_surnames() {

    // }
    public function get_description() {
        return $this->description;
    }

    public function get_supervisor() {
        return $this->supervisor;
    }

    public function get_media_items() {
        return $this->media_items;
    }

    public function get_external_link() {
        return $this->external_link;
    }

    public function get_external_link_text() {
        return $this->external_link_text;
    }

    public function get_thumbnail() {
        return $this->thumbnail;
    }

    public function get_thumbnail_animation() {
        return $this->thumbnail_animation;
    }

    public function get_award() {
        return $this->award;
    }
}


