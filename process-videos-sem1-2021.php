<?php
    require_once 'utils.php';

    $dir = './images/video';

    if (is_dir($dir)) {
        foreach (glob("$dir/*.mp4") as $path) {
            $filename = str_replace("$dir/", '', $path);
            $filename = string_to_filename($filename);
            $new_path = "$dir/$filename";
            rename($path, $new_path);
        }

        foreach (glob("$dir/*.webm") as $path) {
            $filename = str_replace("$dir/", '', $path);
            $filename = string_to_filename($filename);
            $new_path = "$dir/$filename";
            rename($path, $new_path);
        }        
    }
