<?php
    require_once 'utils.php';

    $dir = './images/2021/semester-1';
    // $dir = './images/late-s1y2021';

    if (is_dir($dir)) {
        foreach (glob("$dir/*.jpg") as $path) {
            $filename = str_replace("$dir/", '', $path);
            $filename = string_to_filename($filename);
            $new_path = "$dir/$filename";
            rename($path, $new_path);

            $webp_path = preg_replace("/jpg$/", 'webp', $new_path);
            `~/libwebp-0.4.1-linux-x86-64/bin/cwebp -q 60 -m 6 -mt -resize 600 600 $new_path -o $webp_path`;
        }
    }
