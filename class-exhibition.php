<?php

class Exhibition {
    private $year;
    private $semester;
    private $projects;

    function __construct($year, $semester, $projects = []) {
        $this->year = $year ?? 0;
        $this->semster = $semester ?? 0;
        $this->projects = $projects;
    }

    public function get_year() {
        return $this->year;
    }

    public function get_semester() {
        return $this->semester;
    }

    public function get_projects() {
        return $this->projects;
    }
}
