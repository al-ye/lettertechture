<?php
    // echo print_r($argv, true);
    if (isset($argv[1]) && !defined('BUILD_TARGET')) {
        define('BUILD_TARGET', $argv[1]);
    }

    if (!defined('ABSPATH')) {
        define('ABSPATH', __DIR__);
    }

    if (!defined('PARTIALS')) {
        define('PARTIALS', ABSPATH . '/src/php/partials');
    }

    if (!defined('DATA')) {
        define('DATA', ABSPATH . '/data');
    }

    // if (!defined('BUILD_TARGET')) {
    //     else {
    //         define('BUILD_TARGET', 'DEVELOPMENT');
    //     }
    // }

    if (!defined('INITIAL_LIFECYCLE_STATE')) {
        // define('INITIAL_LIFECYCLE_STATE', 'intro');
        define('INITIAL_LIFECYCLE_STATE', 'steadystate');
        // define('INITIAL_LIFECYCLE_STATE', 'countdown');
    }

    require_once 'version.php';
    require_once 'base-config.php';

    // remove_old_html(BUILD_DIR);

    // function recursive_purge_html($dir) {
    //     if (is_dir($dir)) {
    //         array_map('unlink', glob("$dir/*.html"));
    //         foreach (glob("$dir/*") as $item) {
    //             if (is_dir($item)) {
    //                 recursive_purge_html($item);
    //             }
    //         }
    //     }
    // }


    // require "build-home.php";
    // require "build-major-projects.php";


    // require "s2y2020-build-exhibition.php";
    // require "s2y2020-build-major-projects.php";

    error_log("build.php");
    require "s1y2021-build-exhibition.php";
    require "s1y2021-build-major-projects.php";

    require "build-home.php";
