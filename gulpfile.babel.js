"use strict";

const { series, parallel, src, dest, watch, lastRun } = require('gulp');

const child_process = require('child_process');
const del = require('del');
const browsersync = require('browser-sync').create();
const gulpSass = require('gulp-sass');
const Fiber = require('fibers');
const autoprefixer = require('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const uglify = require('gulp-uglify');
const rename = require("gulp-rename");
const babel = require("gulp-babel");
const rollup = require('gulp-better-rollup');
const rollupBabel = require('rollup-plugin-babel');
const resolve = require('@rollup/plugin-node-resolve');
const commonjs = require('@rollup/plugin-commonjs');
const packageJson = require('./package.json');
const appVersion = packageJson.version || '';
// const Stream = require('stream');
const replace = require('@rollup/plugin-replace');

gulpSass.compiler = require('sass'); // Dart Sass

const devUrl = 'lettertechture.vdev.net';
const devPort = 3200;
const rootDir = './';
// const server = './dist';
const server = false;
// const themeDir = './dist';
// const blocksDir = `${themeDir}/inc/blocks`;

const srcPath = rootDir + 'src',
    // devPath = rootDir + 'assets',
    devPath = rootDir + 'devbuild/assets',
    prodPath = rootDir + 'dist/assets';

const locate = {
    src: {
        sass: `${srcPath}/sass/**/*.scss`,
        js: `${srcPath}/js/**/*.jsx`,
        jquery: `${srcPath}/js/**/*.jquery`,
        php: [`${rootDir}/*.php`, `${srcPath}/php/**/*.php`],
        images: `${srcPath}/images/**/*.*`,
        fonts: `${srcPath}/fonts/**/*.*`,
        data: `${srcPath}/data/**/*.*`,
        // blocksJS: [`${srcPath}/blocks/**/*.js*`, `!${srcPath}/blocks/**/*.bak`],
        // blocksPHP: `${srcPath}/blocks/**/*.php`,
        // blocksCSS: `${srcPath}/blocks/sass/**/*.scss`,
    },
    dev: {
        css: `${devPath}/css/`,
        js: `${devPath}/js/`,
        jquery: `${devPath}/js/`,
        images: `${devPath}/images/`,
        fonts: `${devPath}/webfonts/`,
        data: `${devPath}/data/`,
        // css: `${devPath}/css/`,
        // js: `${devPath}/js/`,
        // jquery: `${devPath}/js/`,
        // images: `${devPath}/images/`,
        // fonts: `${devPath}/webfonts/`,
        // data: `${devPath}/data/`,

    },
    prod: {
        css: `${prodPath}/css/`,
        js: `${prodPath}/js/`,
        jquery: `${prodPath}/js/`,
        images: `${prodPath}/images/`,
        fonts: `${prodPath}/webfonts/`,
        data: `${prodPath}/data/`,
        // blocksJS: `${blocksDir}/`,
        // blocksPHP: `${blocksDir}/`,
        // blocksCSS: `${blocksDir}/`,
    }
};


const vendor = {
    css: [
        // 'node_modules/@fortawesome/fontawesome-pro/css/all.min.css',
        // 'node_modules/flickity/dist/flickity.min.css',
        // 'node_modules/@fortawesome/fontawesome-pro/css/all.css',
        // 'node_modules/@fortawesome/fontawesome-pro/css/fontawesome.css',
        // 'node_modules/@fortawesome/fontawesome-pro/css/brands.css',

    ],
    js: [
        // 'node_modules/jquery/dist/jquery.min.js',
        // 'node_modules/pepjs/dist/pep.js',
        // 'node_modules/jquery.event.swipe/js/jquery.event.swipe.js',
        // 'node_modules/flickity/dist/flickity.pkgd.min.js',
        // 'node_modules/flickity/dist/flickity.pkgd.min.js',
        // 'node_modules/packery/dist/packery.pkgd.min.js',
        // 'node_modules/isotope-layout/dist/isotope.pkgd.min.js',
        // 'node_modules/lazysizes/lazysizes.js',
        // 'node_modules/gsap/src/minified/TweenMax.min.js',

        /* testing */
        // 'node_modules/@fortawesome/fontawesome-pro/js/all.min.js',
        // 'node_modules/@fortawesome/fontawesome-pro/js/brands.min.js',
        // 'node_modules/@fortawesome/fontawesome-pro/js/fontawesome.min.js',
        // 'node_modules/@fortawesome/fontawesome-pro/js/regular.js',
    ]
}



function browserSync(leave) {
    const config = {
        // proxy: devUrl,
        open: false,
        port: devPort,
        ghostMode: {
            clicks: false,
            forms: false,
            scroll: false
        },
        notify: false
    };

    if (server) {
        config.server = server;
    }
    else {
        config.proxy = devUrl;
    }

    browsersync.init(config);

    leave();
}

const browserSyncReload = (leave) => {
    browsersync.reload();

    leave();
}


const _clean = (location) => {
    del.sync(location);
}

const _appendVersion = () => {
    return rename({suffix: '-' + appVersion});
}

const _appendMin = () => {
    return rename(function (file) {
        if (!file.basename.endsWith('.min')) {
            file.basename += '.min';
        }
   });
}

const _removeDirectoryPath = () => {
    return rename({dirname : "" });
}

const _compileSass = () => {
    return src(locate.src['sass'])
        .pipe(gulpSass({
            fiber: Fiber,
            includePaths: ['node_modules/']
        })
        .on('error', gulpSass.logError))
        .pipe(autoprefixer({
            // browsers: ['last 2 versions'],
            cascade: false
        }));
}

const _vendorCSS = (sources, outPath) => {
    return new Promise((resolve, reject) => {
        if (vendor.css.length) {
            src(vendor.css, { since: lastRun(_vendorCSS) })
                .pipe(_removeDirectoryPath())
                .pipe(cssnano())
                .pipe(_appendMin())
                .pipe(dest(outPath))
                // .pipe(browsersync.stream())
                .on('error', reject)
                .on('end', resolve);
        }
        else {
            // leave();
            resolve();
        }
    });
}

const _vendorJS = (sources, outPath) => {
    return new Promise((resolve, reject) => {
        if (vendor.js.length) {
            src(sources, { since: lastRun(_vendorJS) })
                // .on('error', () => {
                //     reject('globbing error');
                // })
                .pipe(_removeDirectoryPath())
                // .pipe(uglify()) // not sure if this will break things that rely on external function/variable names
                // .pipe(_appendMin())
                .pipe(dest(outPath))
                .on('error', reject)
                .on('end', resolve);
        }
        else {
            resolve();
        }
    });
}


const _transpile = () => {
    let plugins = [
        replace({
          'process.env.NODE_ENV': JSON.stringify( 'production' )
        }),
        rollupBabel({
            exclude: 'node_modules/**',
        }),
        resolve(),
        commonjs()
    ];

    return rollup({plugins: plugins}, 'umd');
}


const _copy = (source, destination) => {
    return src(source, { since: lastRun(_copy) })
        .pipe(dest(destination));
}


const _copyImages = (buildTarget) => {
    return src(locate.src.images, { since: lastRun(_copyImages) })
        .pipe(dest(buildTarget.images));
}

const _copyFonts = (buildTarget) => {
    return src(locate.src.fonts, { since: lastRun(_copyFonts) })
        .pipe(dest(buildTarget.fonts));
}

const _copyData = (buildTarget) => {
    return src(locate.src.data, { since: lastRun(_copyData) })
        .pipe(dest(buildTarget.data));
}

const _copyBlocksPhp = (buildTarget) => {
    return _copy(locate.src.blocksPHP, buildTarget.blocksPHP)
}


const _vendorAssets = (buildTarget) => {
    let css = _vendorCSS(vendor.css, buildTarget.css);
    let js = _vendorJS(vendor.js, buildTarget.js);

    return Promise.all([
        css,
        js
    ]);
}

const _jquery = (buildTarget) => {
    return src(locate.src.jquery)
        .pipe(rename({
            extname: '.js'
        }))
        .pipe(_appendVersion())
        // .pipe(dest(buildTarget.jquery))
        .pipe(uglify())
        .on('error', function(err) {
          console.error('Error in uglify task', err.toString());
        })
        .pipe(_appendMin())
        .pipe(dest(buildTarget.jquery));
}


// NOTE: having separate dev/prod functions may be avoidable with conditional chainging
// see (https://stackoverflow.com/questions/40804351/how-do-i-chain-multiple-conditional-promises)
const cleanDev = (leave) => {
    _clean(devPath);
    leave();
}

const cleanBlocks = (leave) => {
    _clean(blocksDir);
    leave();
}

const sassDev = (leave) => {
    return _compileSass()
        .pipe(_appendVersion()) // calls pipe() on stream, passes file data into _appendVersion()
        .pipe(dest(locate.dev['css']))
        .pipe(browsersync.stream()) // otherwise non-minified versions aren't streamed
        .pipe(cssnano())
        .pipe(_appendMin())
        .pipe(dest(locate.dev['css']))
        .pipe(browsersync.stream());
}
const vendorDev = (leave) => {
    return _vendorAssets(locate.dev);
}

const htmlDev = (leave) => {
    child_process.exec('php build.php DEVELOPMENT', leave);
}

// need to fix timing (proper start and end) otherwise page reload triggers too early
const jsDev = (leave) => {
    // const js = src(locate.src.js)
    //     .pipe(_transpile())
    //     .pipe(_appendVersion())
    //     .pipe(dest(locate.dev.js))
    //     .pipe(uglify())
    //     .on('error', function(err) {
    //       console.error('Error in uglify task', err.toString());
    //     })
    //     .pipe(_appendMin())
    //     .pipe(dest(locate.dev.js));

    // return parallel(() => _jquery(locate.dev));

    // _jquery(locate.dev);

    src(locate.src.js)
        .pipe(_transpile())
        .pipe(_appendVersion())
        .pipe(rename({extname: '.js'}))
        .pipe(dest(locate.dev.js))
        .pipe(uglify())
        .on('error', function(err) {
          console.error('Error in uglify task', err.toString());
        })
        .pipe(_appendMin())
        .pipe(dest(locate.dev.js))
        .on('error', leave)
        .on('finish', leave); // not emitted if error
}

const jqueryDev = (leave) => {
    _jquery(locate.dev)
        .on('error', leave)
        .on('finish', leave); // not emitted if error
}

const imagesDev = (leave) => {
    /* note: this approach also works */
    // _copyImages(locate.dev)
    //     .on('error', leave)
    //     .on('finish', leave); // not emitted if error

    return _copyImages(locate.dev);
}
const fontsDev = (leave) => {
    return _copyFonts(locate.dev);
}
const dataDev = (leave) => {
    return _copyData(locate.dev);
}

const copyArchiveDev = (leave) => {
    return _copy()
}



const cleanProd = (leave) => {
    _clean(prodPath);
    leave();
}
const sassProd = (leave) => {
    _compileSass()
        .pipe(cssnano())
        .pipe(_appendVersion())
        .pipe(_appendMin())
        .pipe(dest(locate.prod['css']));
    leave();
}


const vendorProd = (leave) => {
    return _vendorAssets(locate.prod);
}

const jsProd = (leave) => {
    src(locate.src.js)
        .pipe(_transpile())
        .pipe(_appendVersion())
        .pipe(rename({extname: '.js'}))
        .pipe(uglify())
        .on('error', function(err) {
          console.error('Error in uglify task', err.toString());
        })
        .pipe(_appendMin())
        .pipe(dest(locate.prod.js))
        .on('error', leave)
        .on('finish', leave); // not emitted if error
}


const imagesProd = (leave) => {
    return _copyImages(locate.prod);
}
const fontsProd = (leave) => {
    return _copyFonts(locate.prod);
}
const dataProd = (leave) => {
    return _copyData(locate.prod);
}

const htmlProd = (leave) => {
    child_process.exec('php build.php PRODUCTION', leave);
}

// const htmlFP = (leave) => {
//     child_process.exec('php build-temp-frontpage.php', leave);
// }


const logDidRun = (leave) => {
    console.log('Ran!');

    leave();
}

const watchFiles = () => {
    watch(locate.src.sass, series(sassDev));
    watch(locate.src.js, series(jsDev, browserSyncReload));
    watch(locate.src.php, series(htmlDev, browserSyncReload));
    watch(locate.src.images, series(imagesDev, browserSyncReload));
    watch(locate.src.fonts, series(fontsDev, browserSyncReload));
    watch(locate.src.data, series(dataDev, browserSyncReload));
};

// const watchFilesFP = () => {
//     watch(locate.src.sass, series(sassDev, htmlFP, browserSyncReload));
//     watch(locate.src.js, series(jsDev, htmlFP, browserSyncReload));
//     watch(locate.src.php, series(htmlFP, browserSyncReload));
//     // watch(locate.src.images, series(imagesDev, browserSyncReload));
//     // watch(locate.src.fonts, series(fontsDev, browserSyncReload));
//     // watch(locate.src.data, series(dataDev, browserSyncReload));
// };



exports.buildDev = series(
    cleanDev,
    parallel(
        sassDev,
        vendorDev,
        jsDev,
        // jqueryDev,
        imagesDev,
        fontsDev,
        dataDev,
    ),
    htmlDev
);

exports.buildProd = series(
    parallel(
        sassProd,
        vendorProd,
        jsProd,
        imagesProd,
        fontsProd,
        dataProd
    ),
    htmlProd
);

// exports.buildFP = series(
//     cleanDev,
//     parallel(
//         sassDev,
//         jsDev,
//     ),
//     parallel(
//         vendorDev,
//         htmlFP,
//         // jqueryDev,
//         imagesDev,
//         fontsDev,
//         dataDev,
//     )
// );

exports.jsDev = series(jsDev);
exports.sassDev = series(sassDev);

exports.buildAll = series(
    exports.buildDev,
    exports.buildProd,
);


// exports.build = series(
//     cleanDev,
//     parallel(
//         sassDev,
//         vendorDev,
//         jsDev,
//         jqueryDev,
//         imagesDev,
//         fontsDev,
//         dataDev,
//     )

// );

exports.cleanDev = cleanDev;

exports.serve = series(
    // exports.buildDev,
    exports.buildDev,
    browserSync,
    watchFiles
);


// exports.serveFP = series(
//     // exports.buildDev,
//     exports.buildFP,
//     browserSync,
//     watchFilesFP
// );

exports.default = exports.serve;


