<?php

    require_once 'src/php/utils.php';
    require_once 'class-major-project.php';
    $data = './data/sem2-2020-major-projects.csv';
    $headers = [];
    $rows = [];
    if (($handle = fopen($data, "r")) !== FALSE) :
        $limit = 2000;
        $headers = fgetcsv($handle, $limit, ",");
        while (($row_data = fgetcsv($handle, $limit, ",")) !== FALSE) :
            $rows[] = array_combine($headers, $row_data);
        endwhile;
        fclose($handle);
    endif;


    $projects_embed_cache = [];
    $failures = [];

    echo "fetching embeds\n";
    foreach ($rows as $row) {
        $project = new MajorProject($row);
        $id = $project->get_uri();

        $media_items = $project->get_media_items();
        $embeds = [];
        foreach ($media_items as $media) :
            $platform = util_parse_embed_platform($media);
            $request = util_get_oembed_request($media);

            // $html = 'test';
            $html = '';
            try {
                $response = file_get_contents($request);
                $json = json_decode($response);
                $html = $json->html;
            }
            catch (Exception $e) {
                $failures[] = [
                    'id' => $id,
                    'request' => $request,
                    'error' => $e.getMessage()
                ];
            }

            $embeds[] = [
                'request' => $request,
                'platform' => $platform,
                'html' => $html,
            ];

        endforeach;

        echo 'Fetched ' . $project->get_author_display() . "\n";
        // $projects_embed_cache[] = [
        //     'id' => $id,
        //     'embeds' => $embeds,
        // ];

        $projects_embed_cache[$id] = $embeds;
        // $request = util_get_oembed_request($)
    }

    if ($failures) {
        echo "Fetches completed with failures\n";
    }
    else {
        echo "Fetch complete!\n";
    }

    file_put_contents('data/oembed-cache.json', json_encode($projects_embed_cache));
    file_put_contents('embed-fetch.log', json_encode($failures));
