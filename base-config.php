<?php

    if (!defined('BUILD_TARGET')) {
        define('BUILD_TARGET', 'DEVELOPMENT');
    }

    if (!defined('INITIAL_LIFECYCLE_STATE')) {
        define('INITIAL_LIFECYCLE_STATE', 'steadystate');
    }


    require_once 'version.php';

    function is_development_build() {
        return 'DEVELOPMENT' === BUILD_TARGET;
    }

    function is_production_build() {
        return 'PRODUCTION' === BUILD_TARGET;
    }

    if (!defined('BUILD_DIR')) {
        define('BUILD_DIR', is_production_build()
            ? './dist'
            : './devbuild'
        );
    }

    define('PROD', is_production_build());

    define('PROD_URL', 'rmitarchitecture-exhibition.net');
    define('CDN_URL', 'https://d3nfach4wyzdk1.cloudfront.net' );

    define('DEV_ASSETS_URI', '/assets');
    define('ASSETS_URI', is_production_build() ? CDN_URL : DEV_ASSETS_URI);
    define('HOST', is_production_build()
        ? 'https://rmitarchitecture-exhibition.net/'
        : ""
    );

