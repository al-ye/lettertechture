<?php
    $input_path = './data/test-data.csv';
    $headers = [];
    $rows = [];
    if (($handle = fopen($input_path, "r")) !== FALSE) :
        $limit = 800;
        $headers = fgetcsv($handle, $limit, ",");

        for ($i = 0; $i < count($headers); $i++) :
            $formatted = strtolower($headers[$i]);
            $formatted = str_replace(' ', '_', $formatted);
            $headers[$i] = $formatted;
        endfor;

        while (($row_data = fgetcsv($handle, $limit, ",")) !== FALSE) :
            $rows[] = array_combine($headers, $row_data);
        endwhile;
        fclose($handle);
    endif;

    ?>
            <div class="tile-list-section">
                <nav class="filters">
                    <h2 class="screen-reader-text">Filters</h2>
                    <ul class="filter-list">
                        <li class="filter-item filter-item--selected">
                            <button class="filter-item__trigger" data-filter="*">View All<span class="count"> (<?= count($rows)?>)</span></button>
                        </li>

                <?php
                    $categories = [];
                    foreach ($rows as $tile) :
                        $category = $tile['category'];
                        if (!array_key_exists($category, $categories)) :
                            $categories[$category] = 1;
                        else :
                            $categories[$category]++;
                        endif;
                    endforeach;

                    foreach ($categories as $category => $count) :
                        $slug = strtolower($category);
                        $slug = str_replace(' ', '_', $slug);

                        echo '<li class="filter-item">';
                        echo "<button class=\"filter-item__trigger\" data-filter=\"$slug\">$category<span class=\"count\"> ($count)</span></button>";
                        echo '</li>';
                    endforeach;
                    ?>
                    </ul>
                    <ul class="socials-list">
                        <li class="socials-item">
                            <a href="https://www.instagram.com/rmitarchitecture/" class="socials-item__link link link--no-style" target="_blank">
                                <!-- <i class="fab fa-instagram socials-item__icon"></i> -->
                                <i class="fab fa-instagram-square socials-item__icon"></i>
                                <span class="screen-reader-text">Instagram account</span>
                            </a>
                        </li>
                        <li class="socials-item">
                            <a href="https://www.facebook.com/rmitarchitecture/" class="socials-item__link link link--no-style" target="_blank">
                                <!-- <i class="fab fa-facebook-f socials-item__icon"></i> -->
                                <i class="fab fa-facebook-square socials-item__icon"></i>
                                <span class="screen-reader-text">Facebook account</span>
                            </a>
                        </li>
                        <li class="socials-item">
                            <a href="https://architecture.rmit.edu.au/" class="socials-item__link link link--no-style" target="_blank">
                                <i class="fal fa-globe socials-item__icon"></i>
                                <span class="screen-reader-text">RMIT Architecture website</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <ul class="tile-list">
                    <li class="tile-list-item tile-list-item--welcome-video" data-filter-category="ZZsticky">
                        <div class="tile tile--star modal-trigger">
                            <picture class="tile__image ">
                                <source data-srcset="<?= CDN_URL ?>/images/speeches_awards.webp" type="image/webp">
                                <source data-srcset="<?= CDN_URL ?>/images/speeches_awards.jpg" type="image/jpeg">
                                <img class="ZZtile__image lazyload" data-src="<?= CDN_URL ?>/images/speeches_awards.jpg" alt="">
                            </picture>
                            <noscript>
                                <picture class="ZZtile__image">
                                    <source data-srcset="<?= CDN_URL ?>/images/speeches_awards.webp" type="image/webp">
                                    <source data-srcset="<?= CDN_URL ?>/images/speeches_awards.jpg" type="image/jpeg">
                                    <img class="ZZtile__image lazyload" data-src="<?= CDN_URL ?>/images/speeches_awards.jpg" alt="">
                                </picture>
                            </noscript>
                            <div class="tile__content-lockup">
                                <h2 class="tile__primary">Exhibition Opening and Speeches</h2>
                                <p class="tile__secondary"></p>
                            </div>
                            <!-- <a href="#" class="tile__link"></a> -->
                        </div>
                    </li>
                    <li class="tile-list-item tile-list-item--studio-minute" data-filter-category="sticky">
                        <div class="tile tile--star">
                            <picture class="tile__image ">
                                <source data-srcset="<?= CDN_URL ?>/images/studio_in_a_minute.webp" type="image/webp">
                                <source data-srcset="<?= CDN_URL ?>/images/studio_in_a_minute.jpg" type="image/jpeg">
                                <img class="ZZtile__image lazyload" data-src="<?= CDN_URL ?>/images/studio_in_a_minute.jpg" alt="">
                            </picture>
                            <noscript>
                                <picture class="ZZtile__image">
                                    <source data-srcset="<?= CDN_URL ?>/images/studio_in_a_minute.webp" type="image/webp">
                                    <source data-srcset="<?= CDN_URL ?>/images/studio_in_a_minute.jpg" type="image/jpeg">
                                    <img class="ZZtile__image lazyload" data-src="<?= CDN_URL ?>/images/studio_in_a_minute.jpg" alt="">
                                </picture>
                            </noscript>
                            <div class="tile__content-lockup">
                                <h2 class="tile__primary">Studio In a Minute</h2>
                                <p class="tile__secondary"></p>
                            </div>
                            <a href="https://linktr.ee/RMITArchitectureSIM" class="tile__link" target="_blank" rel="nofollow noreferrer"></a>
                        </div>
                    </li>

        <?php
            $index = 0;
            foreach ($rows as $tile) :
                $index++;

                $secondary = trim($tile['secondary_text']);
                $primary = trim($tile['primary_text']);
                $link = $tile['link_to_work'];
                $shape = empty($tile['award']) ? 'circle' : 'star';

                $category = strtolower($tile['category']);
                $category = str_replace(' ', '_', $category);
                $tertiary = $tile['category_shorthand'];
                $image = $tile['image'];
                if (empty($image)) :
                    $image = 'stephen_colour.jpg';
                endif;

                $image = strtolower($image);
                // $image = preg_replace('/(\s*-\s*)|\s+/', '_', $image);
                // $image str_replace('-')
                $image = preg_replace('/\.png$/', '.jpg', $image);
                $image = preg_replace('/\(|\)|-|\s/', '_', $image);
                $image = preg_replace('/_+/', '_', $image);

                // $image = str_replace('.jpg', '.webp', $image);
                // $image = 'images/' . $image;
                $image = CDN_URL . "/images/$image";

                // html_var_dump($image);

                // $srcset = ""
                // $sizes = "("



                // Dev values
                // $shape = rand(1,10) === 5 ? 'star' : 'circle';
                // $image = '/assets/images/stephen-colour.jpg';
                if (!$link) {
                    $link = '';
                }

                $labelledby = "tile-{$index}-title";
                ?>

                    <li class="tile-list-item" data-filter-category="<?= $category ?>">
                        <div class="tile tile--<?= $shape ?>">
                            <picture class="tile__image ">
                                <source data-srcset="<?= str_replace('.jpg', '.webp', $image); ?>" type="image/webp">
                                <source data-srcset="<?= $image ?>" type="image/jpeg">
                                <img class="ZZtile__image lazyload" data-src="<?= $image ?>" alt="">
                            </picture>
                            <noscript>
                                <picture class="ZZtile__image">
                                    <source srcset="<?= str_replace('.jpg', '.webp', $image); ?>" type="image/webp" >
                                    <source srcset="<?= $image ?>" type="image/jpeg">
                                    <img class="tile__image" src="<?= $image ?>" alt="">
                                </picture>
                            </noscript>
                            <div class="tile__content-lockup">
                            <h2 id="<?= $labelledby ?>" class="tile__primary"><?= $primary ?></h2>
                            <p class="tile__secondary"><?= $secondary ?></p>
                            <p class="tile__tertiary"><?= $tertiary ?></p>
                        </div>

                        <?php if (!empty($link)) : ?>

                               <a href="<?= $link ?>"
                                class="tile__link"
                                target="_blank"
                                rel="nofollow noreferrer"
                                aria-labelledby="<?= $labelledby ?>"
                                ></a>
                        <?php endif ?>
                        </div>
                    </li>

        <?php
            endforeach;
            ?>
                </ul>
            </div>
