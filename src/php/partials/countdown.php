            <div class="countdown countdown--hidden">
                <p class="countdown-element countdown-element--hours">
                    <span class="countdown__value">0</span>
                    <span class="countdown__label">Hours</span>
                </p>
                <p class="countdown-element countdown-element--minutes">
                    <span class="countdown__value">0</span>
                    <span class="countdown__label">Minutes</span>
                </p>
                <p class="countdown-element countdown-element--seconds">
                    <span class="countdown__value">0</span>
                    <span class="countdown__label">Seconds</span>
                </p>
            </div>
