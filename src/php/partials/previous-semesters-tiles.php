<?php

    $semesters = [
        [
            'semester' => 1,
            'year' => 2020,
            'url' => '/2020/semester-1',
            'img' => 'semester12020',
        ],
        [
            'semester' => 2,
            'year' => 2020,
            'url' => '/2020/semester-2',
            'img' => 'semester2-2020',
        ],
        // [
        //     'semester' => 1,
        //     'year' => 2021,
        //     'url' => '/2021/semester-1',
        //     'img' => '',
        // ],
    ];

    foreach ($semesters as $semester) :
        [
            'semester' => $s,
            'year' => $y,
            'url' => $url,
            'img' => $img
        ] = $semester;

        // $imgSrc = CDN_URL . "/bucket/images/{$y}/semester-{$s}/{$img}";
        $imgSrc = CDN_URL . "/bucket/images/{$img}";
        $title = "Semester $s<br>$y";

        ?>


                    <li class="tile-list-item tile-list-item--studio-minute" data-filter-category="*">
                        <div class="tile tile--star">
                            <picture class="tile__image ">
                                <source data-srcset="<?= $imgSrc ?>.webp" type="image/webp">
                                <source data-srcset="<?= $imgSrc ?>.jpg" type="image/jpeg">
                                <img class="lazyload" data-src="<?= $imgSrc ?>.jpg" alt="">
                            </picture>
                            <noscript>
                                <picture class="">
                                    <source srcset="<?= $imgSrc ?>.webp" type="image/webp">
                                    <source srcset="<?= $imgSrc ?>.jpg" type="image/jpeg">
                                    <img class=" lazyload" src="<?= $imgSrc ?>.jpg" alt="">
                                </picture>
                            </noscript>
                            <div class="tile__content-lockup">
                                <h2 class="tile__primary"><?= $title ?></h2>
                                <p class="tile__secondary"></p>
                            </div>
                            <?php if ($url) : ?>
                            <a href="<?= $url ?>" class="tile__link" rel="nofollow noreferrer"></a>
                            <?php endif; ?>
                        </div>
                    </li>


<?php
    endforeach;
    ?>
