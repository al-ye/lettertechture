        <div class="modal modal--inactive" title="">
            <div class="modal-liner" title="">
                <button class="modal-close-trigger">Close</button>
                <div class="modal-content">
            <?php
                    $origin = "{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['SERVER_NAME']}";
                    $options = '?fs=1&playsinline=0&modestbranding=1&enablejsapi=1&origin=' . $origin;

                ?>
                    <iframe
                        id="welcome-video"
                        src="https://www.youtube.com/embed/rqP0zUiZwIg<?= $options ?>"
                        class="youtube-embed"
                        type="text/html"
                        width="640"
                        height="360"
                        frameborder="0"
                        ></iframe>
                </div>
            </div>
        </div>
