
            <footer class="page-footer"></footer>
            <?php require PARTIALS . '/footer-logo.php' ?>
<!--             <div class="rmitarch-logo-wrap">
                <svg class="rmitarch-logo" preserveAspectRatio="xMinYMin meet" viewBox="0 0 568.09 73">
                    <use xlink:href="#rmitarch-logo" href="#rmitarch-logo" />
                </svg>
            </div> -->
        </div><!-- .body-content-area -->
    </div><!-- .body-liner -->
    <div class="colophon">
        <span class="colophon-item">Copyright © <?= date('Y') ?>  RMIT University</span>
        <span class="colophon-item"><a href="https://www.rmit.edu.au/utilities/terms" target="_blank" rel="nofollow noreferrer">Terms</a></span>
        <span class="colophon-item"><a href="https://www.rmit.edu.au/utilities/privacy" target="_blank" rel="nofollow noreferrer">Privacy</a></span>
        <span class="colophon-item"><a href="https://www.rmit.edu.au/utilities/accessibility" target="_blank" rel="nofollow noreferrer">Accessibility</a></span>
        <span class="colophon-item"><a href="https://www.rmit.edu.au/utilities/website-feedback" target="_blank" rel="nofollow noreferrer">Website feedback</a></span>
        <span class="colophon-item"><a href="https://www.rmit.edu.au/utilities/complaints" target="_blank" rel="nofollow noreferrer">Complaints</a></span>
        <span class="colophon-item">ABN 49 781 030 034</span>
        <span class="colophon-item">CRICOS provider number: 00122A</span>
        <span class="colophon-item">RTO Code: 3046</span>
    </div>
</body>
</html>

