<?php
    require_once 'utils.php';
    require 'header.php';

    ?>
        <header class="masthead page-header">
            <h1 class="page-title">
                <span class="color--black">RMIT Architecture</span>
                <span class="color--rmit-red">End of semester exhibition</span>
                <span class="color--rmit-red">Semester <?= $_S ?> <span class="date-separator color--black"></span> <?= $_Y ?></span>
            </h1>
            <p class="exhibition-date color--rmit-red">25 June / <span class="exhibition-date__adjustment">6.30PM</span></p>
            <?php require PARTIALS . '/countdown.php' ?>
        </header>
        <main class="page-main">
            <p class="intro-text">The exhibition will launch with the <a href="https://www.facebook.com/rmitarchitecture/" target="_blank" rel="nofollow noreferrer">Opening Event and Awards - Live Stream</a> (below), followed by the reveal of student work from Design Studios, Major Project, Architecture Design Electives and Foundation Design Studios.</p>
            <?php require 'grid.php' ?>
        </main>
        <div class="modal modal--inactive">
            <div class="modal-liner">
                <button class="modal-close-trigger">Close</button>
                <div class="modal-content">
                    <div id="youtube--opening-and-speeches" class="youtube-embed"></div>

                    <script src="https://www.youtube.com/iframe_api" type="text/javascript" async></script>
                    <script type="text/javascript">
                        function onYouTubeIframeAPIReady() {
                           window.introPlayer = new YT.Player('youtube--opening-and-speeches', {
                                height: '390',
                                width: '640',
                                videoId: '1fumsYbyKns',
                            });
                        }
                    </script>
                </div>
            </div>
        </div>

<?php
    require 'footer.php';
