<?php
    // require_once 'build-config.php';
    // require_once 'utils.php';

    $frozen_version = $frozen_version ?: '3.0.0';

?><!DOCTYPE html>
<html
    lang="en_AU" class="no-js"
    data-env="<?= PROD ? 'prod' : 'dev' ?>"
    data-lifecycle="<?= INITIAL_LIFECYCLE_STATE ?>"
    data-semester="s1y2021"
    >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <script>
        document.documentElement.classList.remove('no-js');document.documentElement.classList.add('js');
        window.lazySizesConfig = window.lazySizesConfig || {};lazySizesConfig.init = false;
    </script>
    <title>RMIT Architecture - Exhibitions, Semester <?= "$_S $_Y" ?></title>

    <?php if (PROD) : ?>

    <link rel="preconnect" href="<?= CDN_URL ?>">
    <link rel="dns-prefetch" href="<?= CDN_URL ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <?php endif ?>

    <link rel="stylesheet" href="<?= CDN_URL . "/css/style-s{$_S}y{$_Y}-{$frozen_version}.min.css" ?>">
    <link rel="stylesheet" href="https://use.typekit.net/xoe5mni.css">
    <script src="<?= CDN_URL . "/js/main-s{$_S}y{$_Y}-{$frozen_version}.min.js" ?>" defer></script>

    <script src="https://kit.fontawesome.com/9b4fb7a900.js" crossorigin="anonymous" defer async></script>

    <?php
        if (PROD) :
            require PARTIALS . '/tracking-scripts.php';
        endif;
        ?>

    <script type="text/javascript" src="https://e.issuu.com/embed.js" async="true"></script>
</head>
<body>
<?php require PARTIALS . '/inline-svg.php' ?>
    <div class="body-liner">
        <div class="body-content-area">
