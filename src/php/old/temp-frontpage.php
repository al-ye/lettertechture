<?php
    require_once 'utils.php';
    require 'temp-frontpage-header.php';

    ?>
        <header class="masthead page-header">
            <h1 class="page-title">
                <span class="color--black">RMIT Architecture</span>
                <span class="color--rmit-red">End of semester exhibition</span>
                <span class="color--rmit-red">Semester 2 <span class="date-separator color--black"></span> 2020</span>
            </h1>
            <p class="exhibition-date color--rmit-red">13 November / <span class="exhibition-date__adjustment">6.30PM</span></p>
            <div class="countdown ZZcountdown--hidden">
                <p class="countdown-element countdown-element--hours">
                    <span class="countdown__value">0</span>
                    <span class="countdown__label">Hours</span>
                </p>
                <p class="countdown-element countdown-element--minutes">
                    <span class="countdown__value">0</span>
                    <span class="countdown__label">Minutes</span>
                </p>
                <p class="countdown-element countdown-element--seconds">
                    <span class="countdown__value">0</span>
                    <span class="countdown__label">Seconds</span>
                </p>
            </div>
        </header>


<?php
    require 'footer.php';
