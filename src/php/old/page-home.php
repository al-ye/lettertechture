<?php
    require_once 'utils.php';
    require 'header.php';

    ?>
        <header class="masthead page-header">
            <h1 class="page-title">
                <span class="color--black">RMIT Architecture</span>
                <span class="color--rmit-red">End of semester exhibition</span>
                <span class="color--rmit-red">Semester 2 <span class="date-separator color--black"></span> 2020</span>
            </h1>
            <p class="exhibition-date color--rmit-red">13 November / <span class="exhibition-date__adjustment">6.30PM</span></p>
            <div class="countdown ZZcountdown--hidden">
                <p class="countdown-element countdown-element--hours">
                    <span class="countdown__value">0</span>
                    <span class="countdown__label">Hours</span>
                </p>
                <p class="countdown-element countdown-element--minutes">
                    <span class="countdown__value">0</span>
                    <span class="countdown__label">Minutes</span>
                </p>
                <p class="countdown-element countdown-element--seconds">
                    <span class="countdown__value">0</span>
                    <span class="countdown__label">Seconds</span>
                </p>
            </div>
        </header>
        <main class="page-main">
            <p class="intro-text">The exhibition will launch with the <a href="#" class="modal-trigger">Opening Event and Awards</a> (below), followed by the reveal of student work from Design Studios, Major Project and Architecture Design Electives.</p>

            <?php require 'sem2-2020-grid.php' ?>

        </main>
        <div class="modal modal--inactive">
            <div class="modal-liner">
                <button class="modal-close-trigger">Close</button>
                <div class="modal-content">
                    <div id="youtube--opening-and-speeches" class="youtube-embed"></div>

                    <script src="https://www.youtube.com/iframe_api" type="text/javascript" async></script>
                    <script type="text/javascript">
                        function onYouTubeIframeAPIReady() {
                           window.introPlayer = new YT.Player('youtube--opening-and-speeches', {
                                height: '390',
                                width: '640',
                                videoId: 'vidH7_7aThA',
                            });
                        }
                    </script>
                </div>
            </div>
        </div>

<!-- https://youtu.be/vidH7_7aThA -->
<?php
    require 'footer.php';
