<?php
    $input_path = './data/sem2-2020-exhibits.csv';
    $headers = [];
    $rows = [];
    if (($handle = fopen($input_path, "r")) !== FALSE) :
        $limit = 2000;
        $headers = fgetcsv($handle, $limit, ",");

        for ($i = 0; $i < count($headers); $i++) :
            $formatted = strtolower($headers[$i]);
            $formatted = str_replace(' ', '_', $formatted);
            if ($formatted === 'image') {
                $formatted = 'thumbnail';
            }
            elseif ($formatted === 'animation') {
                $formatted = 'thumbnail_animation';
            }
            $headers[$i] = $formatted;
        endfor;

        while (($row_data = fgetcsv($handle, $limit, ",")) !== FALSE) :
            $rows[] = array_combine($headers, $row_data);
        endwhile;
        fclose($handle);
    endif;

    require_once 'class-major-project.php';
    $major_projects = './data/sem2-2020-major-projects.csv';
    if (($handle = fopen($major_projects, "r")) !== FALSE) :
        $limit = 2000;
        $mp_headers = fgetcsv($handle, $limit, ",");

        $mp = [];
        foreach($headers as $header) {
            $mp[$header] = '';
        }
        // html_var_dump($headers);

        // $mp_headers = fgetcsv($handle, $limit, ",");
        while (($row_data = fgetcsv($handle, $limit, ",")) !== FALSE) :
            $mp_data = array_combine($mp_headers, $row_data);
            $project = new MajorProject($mp_data);
            // html_var_dump($project);
            // html_var_dump($project->get_author_display());
            $mp['primary_text'] = $project->get_author_display();
            $mp['secondary_text'] = $project->get_supervisor();
            $mp['link_to_work'] = '/2020/semester-2/major-projects/' . $project->get_uri();
            $mp['category'] = 'Major Projects';
            $mp['category_shorthand'] = 'MP';
            $mp['thumbnail'] = $project->get_thumbnail();
            $mp['thumbnail_animation'] = $project->get_thumbnail_animation();
            $mp['award'] = $project->get_award();
            // $mp['image'] = $mp_data
            // $rows[] = array_combine($headers, $row_data);

            // echo print_r($row_data, true);
            // $ddd = array_combine($headers, $mp);
            // $rows[] = array_combine($headers, $mp);
            $rows[] = $mp;
        endwhile;
        fclose($handle);
    endif;


    $animations = [];
    ?>
            <div class="tile-list-section">
                <nav id="nav" class="nav nav--sticky">
                    <ul class="menu">
                        <li class="menu-item menu-item--selected">
                            <a class="menu-item__action" href="#all"  data-filter="*">View All<span class="count"> (<?= count($rows)?>)</span></a>
                        </li>

                <?php
                    $categories = [];
                    foreach ($rows as $tile) :
                        $category = $tile['category'];
                        if (!array_key_exists($category, $categories)) :
                            $categories[$category] = 1;
                        else :
                            $categories[$category]++;
                        endif;
                    endforeach;

                    foreach ($categories as $category => $count) :
                        $slug = strtolower($category);
                        // $slug = str_replace(' ', '_', $slug);
                        $slug = str_replace(' ', '-', $slug);

                        echo '<li class="menu-item">';
                        echo "<a class=\"menu-item__action\" href=\"#$slug\" data-filter=\"$slug\">$category<span class=\"count\"> ($count)</span></a>";
                        echo '</li>';
                    endforeach;
                    ?>

                    </ul>
                </nav>
                <ul class="socials-list">
                    <li class="socials-item">
                        <a href="https://www.instagram.com/rmitarchitecture/" class="socials-item__link link link--no-style" target="_blank">
                            <!-- <i class="fab fa-instagram socials-item__icon"></i> -->
                            <i class="fab fa-instagram-square socials-item__icon"></i>
                            <span class="screen-reader-text">Instagram account</span>
                        </a>
                    </li>
                    <li class="socials-item">
                        <a href="https://www.facebook.com/rmitarchitecture/" class="socials-item__link link link--no-style" target="_blank">
                            <!-- <i class="fab fa-facebook-f socials-item__icon"></i> -->
                            <i class="fab fa-facebook-square socials-item__icon"></i>
                            <span class="screen-reader-text">Facebook account</span>
                        </a>
                    </li>
                    <li class="socials-item">
                        <a href="https://architecture.rmit.edu.au/" class="socials-item__link link link--no-style" target="_blank">
                            <i class="fal fa-globe socials-item__icon"></i>
                            <span class="screen-reader-text">RMIT Architecture website</span>
                        </a>
                    </li>
                </ul>
<!--                 <div class="show-names-toggle-wrapper">
                    <div class="show-names-toggle-aligner">
                        <label class="show-names-toggle"><input  type="checkbox" name=""> Show names</label>
                    </div>
                </div> -->
                <ul class="tile-list">
                    <li class="tile-list-item tile-list-item--welcome-video" data-filter-category="ZZsticky">
                        <div class="tile tile--star modal-trigger">
                            <picture class="tile__image ">
                                <source data-srcset="<?= CDN_URL ?>/bucket/images/speeches_awards.webp" type="image/webp">
                                <source data-srcset="<?= CDN_URL ?>/bucket/images/speeches_awards.jpg" type="image/jpeg">
                                <img class="ZZtile__image lazyload" data-src="<?= CDN_URL ?>/bucket/images/speeches_awards.jpg" alt="">
                            </picture>
                            <noscript>
                                <picture class="ZZtile__image">
                                    <source data-srcset="<?= CDN_URL ?>/bucket/images/speeches_awards.webp" type="image/webp">
                                    <source data-srcset="<?= CDN_URL ?>/bucket/images/speeches_awards.jpg" type="image/jpeg">
                                    <img class="ZZtile__image lazyload" data-src="<?= CDN_URL ?>/bucket/images/speeches_awards.jpg" alt="">
                                </picture>
                            </noscript>
                            <div class="tile__content-lockup">
                                <h2 class="tile__primary">Exhibition Opening Event and Awards</h2>
                                <p class="tile__secondary"></p>
                            </div>
                            <!-- <a href="#" class="tile__link"></a> -->
                        </div>
                    </li>
                    <li class="tile-list-item tile-list-item--studio-minute" data-filter-category="major-projects">
                        <div class="tile tile--star">
                            <picture class="tile__image ">
                                <source data-srcset="<?= CDN_URL ?>/bucket/images/2020/semester-2/mpcatalogue800.webp" type="image/webp">
                                <source data-srcset="<?= CDN_URL ?>/bucket/images/2020/semester-2/mpcatalogue800.jpg" type="image/jpeg">
                                <img class="ZZtile__image lazyload" data-src="<?= CDN_URL ?>/bucket/images/2020/semester-2/mpcatalogue800.jpg" alt="">
                            </picture>
                            <noscript>
                                <picture class="ZZtile__image">
                                    <source data-srcset="<?= CDN_URL ?>/images/studio_in_a_minute.webp" type="image/webp">
                                    <source data-srcset="<?= CDN_URL ?>/images/studio_in_a_minute.jpg" type="image/jpeg">
                                    <img class="ZZtile__image lazyload" data-src="<?= CDN_URL ?>/images/studio_in_a_minute.jpg" alt="">
                                </picture>
                            </noscript>
                            <div class="tile__content-lockup">
                                <h2 class="tile__primary">Major Project Catalogue<br>Semester 2 2020</h2>
                                <p class="tile__secondary"></p>
                            </div>
                            <a href="https://issuu.com/rmitarchitecture/docs/mp_20catalogue_20sem_202_202020" class="tile__link" target="_blank" rel="nofollow noreferrer"></a>
                        </div>
                    </li>

        <?php
            $index = 0;
            foreach ($rows as $tile) :
                $index++;

                $secondary = trim($tile['secondary_text']);
                $primary = trim($tile['primary_text']);
                $link = $tile['link_to_work'] ?? '';
                $shape = empty($tile['award']) ? 'circle' : 'star';

                $category = strtolower($tile['category']);
                // $category = str_replace(' ', '_', $category);
                $category = str_replace(' ', '-', $category);
                $tertiary = $tile['category_shorthand'];
                $image = $tile['thumbnail'] ?? '';
                if ($image) :
                    $image = string_to_filename($image);
                    $image = preg_replace('/\.png$/', '.jpg', $image);
                    $image = CDN_URL . "/bucket/images/2020/semester-2/$image";
                endif;

                $animation = $tile['thumbnail_animation'] ?? '';
                if ($animation) :
                    $animation = string_to_filename($animation);
                    $animation = preg_replace('/\.(mov|gif)$/', '.mp4', $animation);
                endif;



                // if (empty($image)) :
                //     $image = 'stephen_colour.jpg';
                // else :
                //     $image = string_to_filename($image);
                // endif;

                // $image = preg_replace('/\.png$/', '.jpg', $image);
                // $image = CDN_URL . "/bucket/images/2020/semester-2/$image";


                // Dev values
                // $shape = rand(1,10) === 5 ? 'star' : 'circle';
                // $image = '/assets/images/stephen-colour.jpg';

                $labelledby = "tile-{$index}-title";
                ?>

                    <li class="tile-list-item" data-filter-category="<?= $category ?>">
                        <div class="tile tile--<?= $shape ?><?php if ($animation) echo ' tile--has-animation';?>">
                            <picture class="tile__image ">
                                <source data-srcset="<?= str_replace('.jpg', '.webp', $image); ?>" type="image/webp">
                                <source data-srcset="<?= $image ?>" type="image/jpeg">
                                <img class="ZZtile__image lazyload" data-src="<?= $image ?>" alt="">
                            </picture>
                        <?php
                            if ($animation) :
                                ?>
                             <video class="tile__animation" loop muted playsinline>
                                <source src="<?= CDN_URL . "/bucket/videos/" . preg_replace('/\.mp4$/', '-H265.mp4', $animation); ?>"
                                        type="video/mp4; codecs=hvc1"
                                        >
                                <source src="<?= CDN_URL . "/bucket/videos/" . preg_replace('/\.mp4$/', '.webm', $animation); ?>"
                                        type="video/webm; codecs=vp9"
                                        >
                                <source src="<?= CDN_URL . "/bucket/videos/$animation"; ?>"
                                        type="video/mp4;"
                                        >
                            </video>

                            <!-- <video src="<?= CDN_URL . "/bucket/videos/$animation"; ?>" class="tile__animation ZZlazyload" loop muted playsinline></video> -->

                        <?php
                            endif;
                            ?>
                            <noscript>
                                <picture class="ZZtile__image">
                                    <source srcset="<?= str_replace('.jpg', '.webp', $image); ?>" type="image/webp" >
                                    <source srcset="<?= $image ?>" type="image/jpeg">
                                    <img class="tile__image" src="<?= $image ?>" alt="">
                                </picture>
                            </noscript>
                            <div class="tile__content-lockup">
                            <h2 id="<?= $labelledby ?>" class="tile__primary"><?= $primary ?></h2>
                            <p class="tile__secondary"><?= $secondary ?></p>
                            <p class="tile__tertiary"><?= $tertiary ?></p>
                        </div>

                        <?php if (!empty($link)) : ?>

                               <a href="<?= $link ?>"
                                class="tile__link"
                                rel="nofollow noreferrer"
                                aria-labelledby="<?= $labelledby ?>"
                            <?php if ('major-projects' !== $category) : echo "target='_blank'"; endif; ?>
                                ></a>
                        <?php endif ?>
                        </div>
                    </li>

        <?php
            endforeach;

            // html_var_dump($animations);
            ?>
                </ul>
            </div>
            <div class="show-names-toggle-wrapper">
                <div class="show-names-toggle-aligner">
                    <label class="show-names-toggle"><input  type="checkbox" name=""> Show names</label>
                </div>
            </div>
