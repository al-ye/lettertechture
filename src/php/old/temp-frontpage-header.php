<!DOCTYPE html>
<html
    lang="en_AU" class="no-js"
    data-env="<?= PROD ? 'prod' : 'dev' ?>"
    data-lifecycle="<?= INITIAL_LIFECYCLE_STATE ?>"
    >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <script>document.documentElement.classList.remove('no-js');document.documentElement.classList.add('js');</script>
    <?php if (PROD) : ?>

    <link rel="preconnect" href="<?= CDN_URL ?>">
    <link rel="dns-prefetch" href="<?= CDN_URL ?>">
    <title>RMIT Architecture - Exhibitions, Semester 2 2020</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <?php
        $css = file_get_contents("dist/devassets/css/temp-frontpage-$version.min.css");
        if ($css) :
            echo "<style>$css</style>";
        endif;

        $js = file_get_contents("dist/devassets/js/temp-frontpage-$version.min.js");
        if ($js) :
            echo "\n\t<script>$js</script>";
        endif;
        ?>

    <?php else : ?>

    <title>Lettertechture</title>
    <link rel="stylesheet" href="/devassets/css/style-<?= $version ?>.min.css">
    <!-- <link rel="stylesheet" href="<?= ASSETS_URI ?>/css/style-<?= $version ?>.min.css"> -->

    <?php endif ?>

    <link rel="stylesheet" href="https://use.typekit.net/xoe5mni.css">

    <!-- <script src="https://kit.fontawesome.com/9b4fb7a900.js" crossorigin="anonymous" defer async></script> -->

<!--     <script type="text/javascript">
        window.lazySizesConfig = window.lazySizesConfig || {};
        lazySizesConfig.init = false;
    </script> -->

    <?php if (PROD) : ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-170909685-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-170909685-1');
    </script>
    <?php endif ?>
</head>
<body>
<?php require 'inline-svg.php';?>
    <div class="body-liner">
        <div class="body-content-area">
