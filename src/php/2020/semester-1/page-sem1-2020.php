<?php
    require_once 'inline-svg.php';
    ?>

<div class="body-liner">
    <div class="body-content-area">
        <header class="masthead page-header">
            <h1 class="page-title">
                <span class="color--black">RMIT Architecture</span>
                <span class="color--rmit-red">End of semester exhibition</span>
                <span class="color--rmit-red">Semester 1 2020</span>
            </h1>
            <p class="exhibition-date color--rmit-red">26 June / <span class="exhibition-date__adjustment">6.30PM</span></p>
            <?php require 'countdown.php' ?>
        </header>
        <main class="page-main">
            <p class="intro-text">The exhibition will launch with the <a href="#" class="modal-trigger">Opening and Speeches</a> (below), followed by the reveal of student work from Design Studios, Major Project and Architecture Design Electives.</p>

            <?php require 'grid.php' ?>

        </main>
        <footer class="page-footer"></footer>
        <div class="rmitarch-logo-wrap">
            <svg class="rmitarch-logo" preserveAspectRatio="xMinYMin meet" viewBox="0 0 568.09 73">
                <use xlink:href="#rmitarch-logo" href="#rmitarch-logo" />
            </svg>
        </div>

    </div><!-- .body-content-area -->
</div><!-- .body-liner -->

<div class="modal modal--inactive">
    <div class="modal-liner">
        <button class="modal-close-trigger">Close</button>
        <div class="modal-content">
            <div id="youtube--opening-and-speeches" class="youtube-embed"></div>

            <script src="https://www.youtube.com/iframe_api" type="text/javascript" async></script>
            <script type="text/javascript">
                function onYouTubeIframeAPIReady() {
                   window.introPlayer = new YT.Player('youtube--opening-and-speeches', {
                        height: '390',
                        width: '640',
                        videoId: 'rqP0zUiZwIg',
                    });
                }
            </script>
        </div>
    </div>
</div>

