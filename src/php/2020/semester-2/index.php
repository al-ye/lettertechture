<?php
    require_once ABSPATH . '/utils.php';
    require 'header--frozen.php';
    // require 'header.php';

    ?>
        <header class="masthead page-header">
            <h1 class="page-title">
                <span class="color--black">RMIT Architecture</span>
                <span class="color--rmit-red">End of semester exhibition</span>
                <span class="color--rmit-red">Semester 2 <span class="date-separator color--black"></span> 2020</span>
            </h1>
            <!-- <p class="exhibition-date color--rmit-red">13 November / <span class="exhibition-date__adjustment">6.30PM</span></p> -->
            <?php //require PARTIALS . '/countdown.php' ?>
        </header>
        <main class="page-main">
            <!-- <p class="intro-text">The exhibition will launch with the <a href="#" class="modal-trigger">Opening Event and Awards</a> (below), followed by the reveal of student work from Design Studios, Major Project and Architecture Design Electives.</p> -->

            <?php require 'grid.php' ?>

        </main>
        <div class="modal modal--inactive">
            <div class="modal-liner">
                <button class="modal-close-trigger">Close</button>
                <div class="modal-content">
                    <div id="youtube--opening-and-speeches" class="youtube-embed"></div>

                    <script src="https://www.youtube.com/iframe_api" type="text/javascript" async></script>
                    <script type="text/javascript">
                        function onYouTubeIframeAPIReady() {
                           window.introPlayer = new YT.Player('youtube--opening-and-speeches', {
                                height: '390',
                                width: '640',
                                videoId: 'vidH7_7aThA',
                            });
                        }
                    </script>
                </div>
            </div>
        </div>

<!-- https://youtu.be/vidH7_7aThA -->
<?php
    require 'footer.php';
