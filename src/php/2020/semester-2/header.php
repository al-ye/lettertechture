<!DOCTYPE html>
<html
    lang="en_AU" class="no-js"
    data-env="<?= PROD ? 'prod' : 'dev' ?>"
    data-lifecycle="<?= INITIAL_LIFECYCLE_STATE ?>"
    data-semester="s2y2020"
    >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <script>document.documentElement.classList.remove('no-js');document.documentElement.classList.add('js');</script>

    <link rel="preconnect" href="<?= CDN_URL ?>">
    <link rel="dns-prefetch" href="<?= CDN_URL ?>">
    <title>RMIT Architecture - Exhibitions, Semester 2 2020</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
<!--     <link rel="prefetch" href="<?= CDN_URL ?>/images/speeches_awards.webp">
    <link rel="prefetch" href="<?= CDN_URL ?>/images/speeches_awards.jpg">
    <link rel="prefetch" href="<?= CDN_URL ?>/images/studio_in_a_minute.webp">
    <link rel="prefetch" href="<?= CDN_URL ?>/images/studio_in_a_minute.jpg"> -->

    <link rel="stylesheet" href="<?= ASSETS_URI . "/css/style-s{$_S}y{$_Y}-{$version}.min.css" ?>">
    <link rel="stylesheet" href="https://use.typekit.net/xoe5mni.css">
    <!-- <script src="<?= ASSETS_URI ?>/js/index-<?= $version ?>.min.js" defer></script> -->
    <script src="<?= ASSETS_URI . "/js/main-s{$_S}y{$_Y}-{$version}.min.js" ?>" defer></script>

    <script src="https://kit.fontawesome.com/9b4fb7a900.js" crossorigin="anonymous" defer async></script>

    <script type="text/javascript">
        window.lazySizesConfig = window.lazySizesConfig || {};
        lazySizesConfig.init = false;
    </script>

    <?php if (PROD) : PARTIALS . '/tracking-scripts.php'; endif; ?>

    <script type="text/javascript" src="https://e.issuu.com/embed.js" async="true"></script>
</head>
<body>
<?php require PARTIALS . '/inline-svg.php';?>
    <div class="body-liner">
        <div class="body-content-area">
