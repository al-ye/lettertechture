<?php
    require_once 'utils.php';
    require 'header-major-project--frozen.php';
    $exhibition_url = "/{$_Y}/semester-{$_S}";

    ?>
        <header class="masthead page-header">
            <p class="page-title">
                <span class="color--black">RMIT Architecture</span>
                <span class="color--rmit-red">End of semester exhibition</span>
                <span class="color--rmit-red">Semester <?= $_S ?> <span class="date-separator color--black"></span> <?= $_Y ?></span>
            </p>
        </header>
        <nav id="nav" class="nav nav--sticky">
            <ul class="menu">
                <li class="menu-item">
                    <a class="menu-item__action" href="<?= $exhibition_url ?>">View Exhibition</a>
                </li>
                <li class="menu-item">
                    <a class="menu-item__action" href="<?= "$exhibition_url/#major-projects"?>">Major Projects<span class="count"> (73)</span></a>
                </li>
            </ul>
        </nav>
        <main class="page-main">
            <div class="project-header">
                <h1 class="project-title">
            <?php
                $title_html = $project->get_title();
                if ($project->get_subtitle()) :
                    $subtitle = $project->get_subtitle();
                    $title_html .= ": <span class=\"project-subtitle\">$subtitle</span>";
                endif;

                echo $title_html;
                ?>
                </h1>
                <p class="project-author"><?= $project->get_author_display() ?></p>
                <p class="project-supervisor">Supervisor: <?= $project->get_supervisor() ?></p>
            <?php
                // $project->get_award();
                if ($project->get_award()) : ?>

                <p class="project-award">Winner of the <?= $project->get_award() ?></p>

            <?php
                endif;
                ?>
            </div>

        <?php
            $media_items = $project->get_media_items();
            $echo = true;

            $oembed_cache = file_get_contents("data/oembed-cache-s{$_S}y{$_Y}.json");
            $oembed_cache = json_decode($oembed_cache, true);

            try {
                $embeds = $oembed_cache[$project->get_uri()];
                foreach ($embeds as $embed) {
                    $html = $embed['html'];
                    $platform = $embed['platform'];
                    echo "<div class='embed-wrapper embed-wrapper--$platform'>";
                    if ($html) {
                        echo $html;
                    }
                    else {
                        $request = $embed['request'];
                        $request = ltrim($request, '?url=');
                        $request = urldecode($request);

                        echo "<iframe src='$request' class='oembed-fallback-iframe'></iframe>";
                    }
                    echo "</div>";
                }
            }
            catch (Exception $e) {

            }

            $description = $project->get_description();
            if ($description) :
                $description = $description; // TODO: add paragraph tags (split by line)
                $lines = explode("\n", $description);
                $html = '';
                foreach($lines as $line) :
                    if (!empty($line)) :
                        $html .= "<p>$line</p>";
                    endif;
                endforeach;
                ?>
            <div>
                <?= $html ?>
            </div>

        <?php
            endif;
            ?>


        <?php
            $url = $project->get_external_link();
            if ($url) :
                $link_text = $project->get_external_link_text() ?? $url;
                ?>

                <a class="project-external-link" href="<?= $url ?>" target="_blank" rel="nofollow noreferrer"><?= $link_text ?></a>
        <?php
            endif;
            ?>

        </main>

<?php
    require 'footer.php';
