console.log('countdown');

window.App = Object.assign({}, window.App, {
    initialised: {
        countdown: false,
    },
    lifecycle: document.documentElement.dataset.lifecycle,
    countdownTimer: undefined
});

const lifecycleStates = {
    countdown: {
        name: 'countdown',
        startTime: new Date(2020, 5, 0)
    },
    intro: {
        name: 'intro',
        // startTime: new Date(2020, 5, 26, 18, 30)
        startTime: new Date(2020, 9, 17, 23, 50, 0),
        utc: {
            year: 2020,
            month: 10,
            date: 13,
            hours: 7,
            minutes: 30,
            seconds: 0
        }
    },
    steadystate: {
        name: 'steadystate',
        // startTime: new Date(2020, 5, 26, 19, 12)
        startTime: new Date(2020, 9, 18, 23, 50, 0)
        // startTime: new Date(2020, 5, 26, 13, 10, 0)
    }
};

const masthead = document.querySelector('.masthead');

window.addEventListener('DOMContentLoaded', initCountdown);

function initCountdown() {
    console.log('init countdown');
    // const utc = createReadout('utc', 'UTC');
    // const local = createReadout('local', 'LOCAL');
    // const melbs = createReadout('melbs', 'MELBOURNE');

    const deadline = new Date();
    deadline.setUTCFullYear(2020);
    deadline.setUTCMonth(10);
    deadline.setUTCDate(13);
    deadline.setUTCHours(7);
    deadline.setUTCMinutes(30);
    deadline.setUTCSeconds(0);
    const deadlineElem = document.createElement('p');
    deadlineElem.innerHTML = `Deadline: ${deadline.toUTCString()}`;
    deadlineElem.style.color = 'red';

    console.log(deadline.toUTCString());

    // masthead.appendChild(deadlineElem);
    // masthead.appendChild(utc);
    // masthead.appendChild(melbs);
    // masthead.appendChild(local);

    tick();

    function tick() {
        const utc = new Date(Date.now());
        let remaining = deadline.getTime() - utc.getTime();

        if (remaining !== 0) {
            remaining = Math.round(remaining / 1000);
        }

        if (remaining >= 0) {
            updateCountdownDisplay(remaining);
        }

        setTimeout(tick, 1000);
    }
}

function updateUTC() {
    document.querySelector('.utc-value').innerHTML = getUTCTime();
}

function getUTCTime() {
    let date = new Date(Date.now());
    return date.toUTCString();
}



// function createReadout(id, label) {
//     const readout = document.createElement('p');
//     readout.classList.add(`${id}-readout`);
//     readout.innerHTML = `${label}: `;
//     const value = document.createElement('span');
//     value.classList.add(`${id}-value`);
//     readout.appendChild(value);

//     return readout;
// }


function updateCountdownDisplay(secondsInterval) {
    let hours = 0;
    let minutes = 0;
    let seconds = 0;

    if (secondsInterval > 0) {
        hours = Math.floor(secondsInterval / (60 * 60));
        minutes = Math.floor((secondsInterval / 60) % 60);
        seconds = Math.floor(secondsInterval % 60);
    }


    document.querySelector('.countdown-element--hours .countdown__value').innerHTML = formatCountdownValue(hours);
    document.querySelector('.countdown-element--minutes .countdown__value').innerHTML = formatCountdownValue(minutes);
    document.querySelector('.countdown-element--seconds .countdown__value').innerHTML = formatCountdownValue(seconds);
}

function formatCountdownValue(value) {
    if (value < 10) {
        value = '0' + value;
    }

    return value;
}
