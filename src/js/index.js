import 'lazysizes';
// import moment from 'moment';


// updateCountdownDisplay(600);

const masthead = document.querySelector('.masthead');

init();



console.log('howdy')

embed();
function embed() {
    const asset = 'https%3A//issuu.com/rmitarchitecture/docs/mp_catalogue_sem_2_2019_v1';
    const oembed = `https://issuu.com/oembed?format=json&url=${asset}`;
    // const oembed = 'https://issuu.com/oembed?format=json&url=https%3A%2F%2Fissuu.com%2Frmitarchitecture%2Fdocs%2Fmp_catalogue_sem_2_2019_v1'
    console.log('fetching embed');
    fetch(oembed)
        .then(response => {
            console.log('response received');
            response.json()
        })
        .then(json => {
            console.log(json);
            console.log('fetch complete');
        });
}



function init() {
    const utc = createReadout('utc', 'UTC');
    const local = createReadout('local', 'LOCAL');
    const melbs = createReadout('melbs', 'MELBOURNE');


    const deadline = new Date();
    deadline.setUTCFullYear(2020);
    deadline.setUTCMonth(9);
    deadline.setUTCDate(17);
    deadline.setUTCHours(18);
    deadline.setUTCMinutes(0);
    deadline.setUTCSeconds(0);
    const deadlineElem = document.createElement('p');
    deadlineElem.innerHTML = `Deadline: ${deadline.toUTCString()}`;
    deadlineElem.style.color = 'red';

    masthead.appendChild(deadlineElem);
    masthead.appendChild(utc);
    masthead.appendChild(melbs);
    masthead.appendChild(local);




    tick();

    // updateUTC();
    // updateLocal();
    // setTimeout(tick, 1000);
    // createTimeout(updateLocal, 1000);

    function tick() {
        updateUTC();
        updateLocal();
        updateMelbourne();

        const utc = new Date(Date.now());
        let remaining = deadline.getTime() - utc.getTime();
        // console.log(remaining)
        if (remaining !== 0) {
            remaining = Math.round(remaining / 1000);
        }

        if (remaining >= 0) {
            updateCountdownDisplay(remaining);
        }

        setTimeout(tick, 1000);
    }
}

function updateUTC() {
    document.querySelector('.utc-value').innerHTML = getUTCTime();
}

function updateLocal() {
    document.querySelector('.local-value').innerHTML = getLocalTime();
}

function updateMelbourne() {
    document.querySelector('.melbs-value').innerHTML = getMelbourneTime();
}

function getLocalTime() {
    let date = new Date(Date.now());
    return date.toLocaleString('en-AU');
    // return date.toLocaleString(navigator.language, {timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone});
}

function getUTCTime() {
    let date = new Date(Date.now());
    return date.toUTCString();
}

function getMelbourneTime() {
    let date = new Date(Date.now());
    return date.toLocaleString('en-AU', {timeZone: "Australia/Melbourne"});
}


function createReadout(id, label) {
    const readout = document.createElement('p');
    readout.classList.add(`${id}-readout`);
    readout.innerHTML = `${label}: `;
    const value = document.createElement('span');
    value.classList.add(`${id}-value`);
    readout.appendChild(value);

    return readout;
}



function initLazySizes() {
    window.lazySizesConfig = window.lazySizesConfig || {};
    // lazySizesConfig.expand = 200;
    lazySizesConfig.loadMode = 2; // 1;
    lazySizes.init();
}

function updateCountdownDisplay(secondsInterval) {
    let hours = 0;
    let minutes = 0;
    let seconds = 0;

    if (secondsInterval > 0) {
        hours = Math.floor(secondsInterval / (60 * 60));
        minutes = Math.floor((secondsInterval / 60) % 60);
        seconds = Math.floor(secondsInterval % 60);
    }


    document.querySelector('.countdown-element--hours .countdown__value').innerHTML = formatCountdownValue(hours);
    document.querySelector('.countdown-element--minutes .countdown__value').innerHTML = formatCountdownValue(minutes);
    document.querySelector('.countdown-element--seconds .countdown__value').innerHTML = formatCountdownValue(seconds);
}

function formatCountdownValue(value) {
    if (value < 10) {
        value = '0' + value;
    }

    return value;
}
