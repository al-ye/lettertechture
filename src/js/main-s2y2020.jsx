import 'lazysizes';

window.App = window.App || {
    initialised: {
        countdown: false,
        filters: false,
        modal: false
    },
    yt: {
        openingAndSpeeches: undefined
    },
    lifecycle: document.documentElement.dataset.lifecycle,
    countdownTimer: undefined
};

const lifecycleStates = {
    countdown: {
        name: 'countdown',
        startTime: new Date(2020, 5, 0)
    },
    intro: {
        name: 'intro',
        // // startTime: new Date(2020, 5, 26, 18, 30)
        // startTime: new Date(2020, 9, 17, 23, 50, 0),
        // utc: {
        //     year: 2020,
        //     month: 10,
        //     date: 13,
        //     hours: 7,
        //     minutes: 30,
        //     seconds: 0
        // }
        startTime: (() => {
            // AEDT 6:30pm
            // UTC 7:30am
            const start = new Date();
            start.setUTCFullYear(2020);
            start.setUTCMonth(10);
            start.setUTCDate(13);
            start.setUTCHours(7);
            start.setUTCMinutes(30);
            start.setUTCSeconds(0);

            return start;
        })()
    },
    steadystate: {
        name: 'steadystate',
        // // startTime: new Date(2020, 5, 26, 19, 12)
        // // startTime: new Date(2020, 5, 26, 13, 10, 0)
        // startTime: new Date(2020, 9, 18, 23, 50, 0)
        startTime: (() => {
            // AEDT 6:30pm
            // UTC 7:30am
            const start = new Date();
            start.setUTCFullYear(2020);
            start.setUTCMonth(10);
            start.setUTCDate(13);
            start.setUTCHours(8);
            start.setUTCMinutes(8);
            start.setUTCSeconds(0);

            return start;
        })()
    }
};

// console.log(lifecycleStates.intro.startTime);

// const masthead = document.querySelector('.masthead');

// window.addEventListener('DOMContentLoaded', initCountdown);



const override = false;
if (override) {
    lifecycleStates.intro.startTime = (() => {
        // AEDT 6:30pm
        // UTC 7:30am
        const start = new Date();
        start.setUTCFullYear(2020);
        start.setUTCMonth(10);
        start.setUTCDate(13);
        start.setUTCHours(4);
        start.setUTCMinutes(18);
        start.setUTCSeconds(0);

        return start;
    })();


    lifecycleStates.steadystate.startTime = (() => {
        // AEDT 6:30pm
        // UTC 7:30am
        const start = new Date();
        start.setUTCFullYear(2020);
        start.setUTCMonth(10);
        start.setUTCDate(13);
        start.setUTCHours(4);
        start.setUTCMinutes(18);
        start.setUTCSeconds(20);

        return start;
    })();
}



function getLocalTime() {
    let date = new Date(Date.now());
    // return date.toLocaleString('en-AU');
    // return date.toLocaleString();
    return date.toLocaleString(navigator.language, {timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone});
}

// const c = document.querySelector('.masthead');
// const local = document.createElement('p');
// local.innerHTML = getLocalTime();
// local.style.fontSize = '2rem';
// c.appendChild(local);



initTimeline();
initLazySizes();

// startLifecycleSteadystate();



// initFilters();
// initModal();
// window.addEventListener('DOMContentLoaded')


// LIFECYCLE FUNCTIONS


function initTimeline() {
    const now = Date.now();
    if (now >= lifecycleStates.steadystate.startTime.getTime()) {
        setLifecycleState('steadystate');
    }
    else if (now >= lifecycleStates.intro.startTime.getTime()) {
        setLifecycleState('intro');
    }
    else {
        setLifecycleState('countdown');
    }
}

function setLifecycleState(state) {
    window.App.lifecycle = state;
    document.documentElement.dataset.lifecycle = state;
    onLifecycleDidChange();
}

function getLifecycleState() {
    return window.App.lifecycle;
}

function onLifecycleDidChange() {
    switch(getLifecycleState()) {
        case 'countdown':
            startLifecycleCountdown();
            break;
        case 'intro':
            startLifecycleIntro();
            break;
        case 'steadystate':
            startLifecycleSteadystate();
            break;
        default:
    }
}

function startLifecycleCountdown() {
    initCountdown();
}

function startLifecycleIntro() {
    // end countdown
    initFilters();
    initModal();

    const now = new Date(Date.now());
    const end = lifecycleStates.steadystate.startTime;
    const timeRemaining = end.getTime() - now.getTime();

    if (timeRemaining > 0) {
        setTimeout(() => {
            setLifecycleState('steadystate');
        }, timeRemaining);
    }
}

function startLifecycleSteadystate() {
    initFilters();
    initModal();
}

// END LIFECYCLE FUNCTIONS




function createInterval(interval, callback, endIntervalCallback) {
    return setInterval(callback, interval, endIntervalCallback);
}

function initCountdown() {
    // bail early if timer already exist
    if (window.App.initialised.countdown) {
        return;
    }

    const interval = getCountdownIterval(lifecycleStates.intro.startTime);

    // don't initialise if countdown is no longer required
    if (interval < 0 ) {
        return;
    }

    onCountdownStart();
    onCountdownTick(stopClock);
    window.App.countdownTimer = createInterval(1000, onCountdownTick, stopClock);

    window.App.initialised.countdown = true;


    function getCountdownIterval(endTime) {
        const now = new Date(Date.now());
        const end = new Date(endTime.getTime());
        const secondsRemaining = Math.ceil((end.getTime() - now.getTime()) / 1000);

        return secondsRemaining;
    }

    function stopClock() {
        clearInterval(window.App.countdownTimer);
        onCountdownEnd();
    }

    function formatCountdownValue(value) {
        if (value < 10) {
            value = '0' + value;
        }

        return value;
    }

    function onCountdownTick(endInterval) {
        const target = lifecycleStates.intro.startTime;
        const timeRemaining = getCountdownIterval(target);
        updateCountdownDisplay(timeRemaining);

        if (timeRemaining < 0) {
            endInterval();
        }
    }

    function onCountdownStart() {
        const countdown = document.querySelector('.countdown');
        countdown.classList.add('countdown--visible');
        countdown.classList.remove('countdown--hidden');
        countdown.classList.add('countdown--is-running');
    }

    function onCountdownEnd() {
        // const countdown = document.querySelector('.countdown');
        // countdown.classList.remove('countdown--is-running');
        // countdown.classList.remove('countdown--visible');
        // countdown.classList.add('countdown--hidden');
        setLifecycleState('intro');
    }


    function updateCountdownDisplay(secondsInterval) {
        let hours = 0;
        let minutes = 0;
        let seconds = 0;

        if (secondsInterval > 0) {
            hours = Math.floor(secondsInterval / (60 * 60));
            minutes = Math.floor((secondsInterval / 60) % 60);
            seconds = Math.floor(secondsInterval % 60);
        }

        document.querySelector('.countdown-element--hours .countdown__value').innerHTML = formatCountdownValue(hours);
        document.querySelector('.countdown-element--minutes .countdown__value').innerHTML = formatCountdownValue(minutes);
        document.querySelector('.countdown-element--seconds .countdown__value').innerHTML = formatCountdownValue(seconds);
    }
}


function initLazySizes() {
    window.lazySizesConfig = window.lazySizesConfig || {};
    // lazySizesConfig.expand = 200;
    lazySizesConfig.loadMode = 2; // 1;
    lazySizes.init();
}


function initFilters() {
    if (window.App.initialised.filters) {
        return;
    }

    const filters = [].slice.call(document.querySelectorAll('.menu-item__action[data-filter]'));
    const tiles = [].slice.call(document.querySelectorAll('.tile-list-item'));
    if (!filters || !filters.length) {
        return;
    }

    if (window.location.hash) {
        const category = window.location.hash.substring(1);
        // clearFilterIndicators();
        // setVisibleTiles(filter);
        const filter = document.querySelector(`.menu-item__action[data-filter=${category}]`);
        setCurrentSelectedFilter(filter);
    }

    filters.forEach(filter => {
        const parent = filter.closest('.menu-item');

        filter.addEventListener('click', (e) => {
            // e.preventDefault();

            // const category = filter.dataset.filter;
            setCurrentSelectedFilter(filter);


            // if (parent && !parent.classList.contains('menu-item--selected')) {
            //     clearFilterIndicators();
            //     parent.classList.add('menu-item--selected');
            // }

            // setVisibleTiles(category);
        });
    });


    // console.log(window.location);

    window.App.initialised.filters = true;

    function setCurrentSelectedFilter(filter) {
        if (!filter) {
            return;
        }

        const parent = filter.closest('.menu-item');
        if (parent && !parent.classList.contains('menu-item--selected')) {
            clearFilterIndicators();
            parent.classList.add('menu-item--selected');
        }

        const category = filter.dataset.filter;
        setVisibleTiles(category);
    }

    function setVisibleTiles(category) {
        // const category = filter.dataset.filter;
        tiles.forEach(tile => {
            if ('*' === category
                || 'sticky' === tile.dataset.filterCategory
                || category === tile.dataset.filterCategory)
                {
                tile.classList.remove('tile-list-item--hidden');
            }
            else {
                tile.classList.add('tile-list-item--hidden');
            }
        });
    }



    function clearFilterIndicators() {
        filters.forEach(filter => {
            const parent = filter.closest('.menu-item');
            if (parent) {
                parent.classList.remove('menu-item--selected');
            }
        });
    }
}


initShowNamesToggle();
function initShowNamesToggle() {
    const toggleElem = document.querySelector('.show-names-toggle');

    if (!toggleElem) {
        return;
    }

    const checkbox = toggleElem.querySelector('input[type=checkbox]');
    const panel = document.querySelector('.show-names-toggle-wrapper');

    const forceOverrideMediaQuery = '(max-width: 1200px)';
    const queryMatchMedia = matchMedia(forceOverrideMediaQuery);

    const decideToggleVisibility = () => {
        if (queryMatchMedia.matches) {
            setNameForceStatus(true);
            hidePanel();
        }
        else {
            setNameForceStatus(checkbox.checked);
            restorePanel();
        }
    }

    queryMatchMedia.addEventListener('change', decideToggleVisibility, {passive: true});
    decideToggleVisibility();

    toggleElem.addEventListener('click', toggleNames, {passive: true});

    function toggleNames(e) {
        if (e.target.type === 'checkbox') {
            setNameForceStatus(e.target.checked);
        }
    }

    function hidePanel() {
        panel.classList.add('hidden');
    }

    function restorePanel() {
        panel.classList.remove('hidden');
    }

    function setNameForceStatus(status) {
        const tileList = [].slice.call(document.querySelectorAll('.tile-list'));
        tileList.forEach(list => {
            list.classList.toggle('force-names', status);
        });
    }
}


initTileAnimations();
function initTileAnimations() {
    const tiles = document.querySelectorAll('.tile--has-animation');
    if (!tiles) {
        return;
    }

    tiles.forEach(tile => {
        const animation = tile.querySelector('.tile__animation');
        if (animation) {
            tile.addEventListener('mouseenter', () => {
                animation.currentTime = 0;
                animation.play();
            });


            tile.addEventListener('mouseout', () => {
                animation.pause();
            });


            const link = tile.querySelector('.tile__link');
            if (link) {
                link.addEventListener('focus', () => {
                    animation.currentTime = 0;
                    animation.play();
                });
                link.addEventListener('blur', () => {
                    animation.pause();
                });
            }

        }

    });

}


function initModal() {
    if (window.App.initialised.modal) {
        return;
    }

    const modal = document.querySelector('.modal');
    if (!modal) {
        return;
    }

    const closeButton = modal.querySelector('.modal-close-trigger');
    closeButton.addEventListener('click', closeModal);

    modal.addEventListener('click', closeModal);

    const triggers = document.querySelectorAll('.modal-trigger');
    triggers.forEach(trigger => {
        trigger.addEventListener('click', openModal);
    });

    window.App.initialised.modal = modal;
}

function openModal() {
    const modal = window.App.initialised.modal;
    modal.classList.remove('modal--inactive');
    modal.classList.add('modal--active');
    try {
        window.introPlayer.playVideo();
    }
    catch(err) {
        console.log(err);
    }
}


function closeModal() {
    const modal = window.App.initialised.modal;
    try {
        window.introPlayer.pauseVideo();
    }
    catch(err) {
        console.log(err);
    }
    modal.classList.remove('modal--active');
    modal.classList.add('modal--inactive');
}


function measureOnResize() {
    const nav = document.getElementById('nav');
    if (nav) {
        document.documentElement.style.setProperty('--nav-height', nav.clientHeight + 'px');
    }
}




window.addEventListener('load', measureOnResize);
window.addEventListener('resize', measureOnResize);
