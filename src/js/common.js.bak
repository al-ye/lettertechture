import 'lazysizes';

window.App = window.App || {
    initialised: {
        countdown: false,
        filters: false,
        modal: false
    },
    yt: {
        openingAndSpeeches: undefined
    },
    lifecycle: document.documentElement.dataset.lifecycle,
    countdownTimer: undefined
};


initLazySizes();
initFilters();
initModal();
// window.addEventListener('DOMContentLoaded')

function initLazySizes() {
    window.lazySizesConfig = window.lazySizesConfig || {};
    // lazySizesConfig.expand = 200;
    lazySizesConfig.loadMode = 2; // 1;
    lazySizes.init();
}


function initFilters() {
    if (window.App.initialised.filters) {
        return;
    }

    const filters = [].slice.call(document.querySelectorAll('.filter-item'));
    const tiles = [].slice.call(document.querySelectorAll('.tile-list-item'));
    if (!filters || !filters.length) {
        return;
    }

    filters.forEach(filter => {
        const trigger = filter.querySelector('.filter-item__trigger');
        trigger.addEventListener('click', () => {
            if (!filter.classList.contains('filter-item--selected')) {
                clearFilterIndicators();
                filter.classList.add('filter-item--selected');

                const category = trigger.dataset.filter;
                tiles.forEach(tile => {
                    if ('*' === category ||
                        'sticky' === tile.dataset.filterCategory ||
                        category === tile.dataset.filterCategory)
                        {
                        tile.classList.remove('tile-list-item--hidden');
                    }
                    else {
                        tile.classList.add('tile-list-item--hidden');
                    }
                });

            }
        });
    });

    window.App.initialised.filters = true;

    function clearFilterIndicators() {
        filters.forEach(filter => {
            filter.classList.remove('filter-item--selected');
        });
    }
}


function initModal() {
    if (window.App.initialised.modal) {
        return;
    }

    const modal = document.querySelector('.modal');
    if (!modal) {
        return;
    }

    const closeButton = modal.querySelector('.modal-close-trigger');
    closeButton.addEventListener('click', closeModal);

    modal.addEventListener('click', closeModal);

    const triggers = document.querySelectorAll('.modal-trigger');
    triggers.forEach(trigger => {
        trigger.addEventListener('click', openModal);
    });

    window.App.initialised.modal = modal;
}

function openModal() {
    const modal = window.App.initialised.modal;
    modal.classList.remove('modal--inactive');
    modal.classList.add('modal--active');
    try {
        window.introPlayer.playVideo();
    }
    catch(err) {
        console.log(err);
    }
}


function closeModal() {
    const modal = window.App.initialised.modal;
    try {
        window.introPlayer.pauseVideo();
    }
    catch(err) {
        console.log(err);
    }
    modal.classList.remove('modal--active');
    modal.classList.add('modal--inactive');
}


function measureOnResize() {
    const nav = document.getElementById('nav');
    if (nav) {
        document.documentElement.style.setProperty('--nav-height', nav.clientHeight + 'px');
    }
}

window.addEventListener('load', measureOnResize);
window.addEventListener('resize', measureOnResize);
