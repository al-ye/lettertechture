<?php

    require_once 'version.php';
    require_once 'base-config.php';

    // $is_home = true;

    $_S = 1;
    $_Y = 2021;

    ob_start();

    require ABSPATH . "/src/php/{$_Y}/semester-{$_S}/index.php";

    $html = ob_get_clean();

    file_put_contents(BUILD_DIR . "/index.html", $html);
