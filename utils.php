<?php
define('ROOT_DIR', __DIR__);


function html_var_dump($dumpable) {
    echo '<div class="var-dump"><pre>';

    var_dump($dumpable);

    echo '</pre></div>';
}

function string_to_filename($string) {
    $sanitized = trim($string);
    $sanitized = strtolower($sanitized);
    // $sanitized = str_replace("'", " ", $sanitized);
    $sanitized = preg_replace("/[^.[:alnum:]]/", '-', $sanitized);
    $sanitized = preg_replace("/-{1,}/", '-', $sanitized);
    $sanitized = preg_replace("/-$/", '', $sanitized);
    $sanitized = preg_replace("/-\.(.*?)$/", '.$1', $sanitized);
    return $sanitized;
}

function util_get_oembed_request($url) {
    $html = '';
    if (!$url) {
        return $html;
    }

    $host = parse_url($url, PHP_URL_HOST);
    if (!$host) {
        return $html;
    }

    $services = [
        'youtube' => [
            'hosts' => ['www.youtube.com', 'youtu.be'],
            'endpoint' => 'https://www.youtube.com/oembed',
        ],
        'vimeo' => [
            'hosts' => ['vimeo.com'],
            'endpoint' => 'https://vimeo.com/api/oembed.json',
        ],
        'issuu' => [
            'hosts' => ['issuu.com'],
            'endpoint' => 'https://issuu.com/oembed',
        ],
        'indesign' => [
            'hosts' => ['indd.adobe.com'],
            'endpoint' => '',
        ],
    ];

    $url_service = null;
    foreach ($services as $service => $service_hosts) {
        foreach ($service_hosts['hosts'] as $service_host) {
            if ($service_host === $host) {
                $url_service = $service;
                break(2);
            }
        }
    }

    $endpoint = $services[$url_service]['endpoint'];
    $oembed_request = $endpoint . '?url=' . urlencode($url);

    return $oembed_request;

}

function util_parse_embed_platform($url) {
    $html = '';
    if (!$url) {
        return $html;
    }

    $host = parse_url($url, PHP_URL_HOST);
    if (!$host) {
        return $html;
    }

    $services = [
        'youtube' => [
            'hosts' => ['www.youtube.com', 'youtu.be'],
            'endpoint' => 'https://www.youtube.com/oembed',
        ],
        'vimeo' => [
            'hosts' => ['vimeo.com'],
            'endpoint' => 'https://vimeo.com/api/oembed.json',
        ],
        'issuu' => [
            'hosts' => ['issuu.com'],
            'endpoint' => 'https://issuu.com/oembed',
        ],
        'indesign' => [
            'hosts' => ['indd.adobe.com'],
            'endpoint' => '',
        ],
    ];

    $url_service = null;
    foreach ($services as $service => $service_hosts) {
        foreach ($service_hosts['hosts'] as $service_host) {
            if ($service_host === $host) {
                $url_service = $service;
                break(2);
            }
        }
    }

    return $url_service;

    // $endpoint = $services[$url_service]['endpoint'];
    // $oembed_request = $endpoint . '?url=' . urlencode($url);

    // return $oembed_request;

}


function init_major_projects_folder($dir) {
    if (!is_dir($dir)) {
        mkdir($dir, 0755, true);
    }
    else {
        foreach (glob("$dir/*") as $item) {
            if (is_dir($item)) {
                array_map('unlink', glob("$item/*.html"));
                rmdir($item);
            }
            else {
                unlink($item);
            }
        }
    }
}
