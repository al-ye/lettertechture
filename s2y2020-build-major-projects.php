<?php
    require_once 'version.php';
    require_once 'base-config.php';

    $_S = 2;
    $_Y = 2020;

    $input_path = "data/sem{$_S}-{$_Y}-major-projects.csv";
    $headers = [];
    $rows = [];
    if (($handle = fopen($input_path, "r")) !== FALSE) :
        $limit = 2000;
        $headers = fgetcsv($handle, $limit, ",");

        for ($i = 0; $i < count($headers); $i++) :
            $formatted = strtolower($headers[$i]);
            $formatted = str_replace(' ', '_', $formatted);
            $headers[$i] = $formatted;
        endfor;

        while (($row_data = fgetcsv($handle, $limit, ",")) !== FALSE) :
            // // error_log(print_r(count($headers) . ' : ' . count($row_data), true));
            // if (count($row_data) !== count($headers)) {
            //     error_log(print_r($row_data, true));
            // }
            // else {
            // }
            $rows[] = array_combine($headers, $row_data);
        endwhile;
        fclose($handle);
    endif;

    foreach ($rows as $row) {
        $row['year'] = $_Y;
        $row['semester'] = $_S;
    }

    if (!empty($rows)) {
        require_once ABSPATH . '/class-major-project.php';
        // $projects_dir = BUILD_DIR . '/major-projects';
        $projects_dir = BUILD_DIR . "/{$_Y}/semester-{$_S}/major-projects";
        init_major_projects_folder($projects_dir);

        foreach ($rows as $row) {

            $project = new MajorProject($row);

            ob_start();

            require ABSPATH . "/src/php/{$_Y}/semester-{$_S}/page-major-project.php";

            $html = ob_get_clean();

            $output_dir = $project->get_uri();
            if (!is_dir($output_dir)) {
                mkdir("$projects_dir/$output_dir");
            }

            file_put_contents("$projects_dir/$output_dir/index.html", $html);
        }
    }


    /* Moved to utils.php */

    // function init_major_projects_folder($dir) {
    //     if (!is_dir($dir)) {
    //         mkdir($dir, 0755, true);
    //     }
    //     else {
    //         foreach (glob("$dir/*") as $item) {
    //             if (is_dir($item)) {
    //                 array_map('unlink', glob("$item/*.html"));
    //                 rmdir($item);
    //             }
    //             else {
    //                 unlink($item);
    //             }
    //         }
    //     }
    // }

