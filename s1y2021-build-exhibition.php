<?php

    require_once 'version.php';
    require_once 'base-config.php';

    // $is_home = true;

    $_S = 1;
    $_Y = 2021;

    ob_start();

    // require 'src/php/page-home.php';
    require ABSPATH . "/src/php/{$_Y}/semester-{$_S}/index.php";

    $html = ob_get_clean();

    // if (!is_dir('dist/temp-frontpage')) {
    //     mkdir('dist/temp-frontpage');
    // }

    // file_put_contents(BUILD_DIR . '/index.html', $html);
    file_put_contents(BUILD_DIR . "/{$_Y}/semester-{$_S}/index.html", $html);
